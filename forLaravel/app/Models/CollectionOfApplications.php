<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CollectionOfApplications extends Model
{
    use HasFactory;

    protected $table = 'collection_of_applications';

    protected $fillable = [
        'id',
        'phone',
        'name'
    ];
}
