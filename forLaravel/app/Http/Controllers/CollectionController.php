<?php

namespace App\Http\Controllers;

use App\Models\CollectionOfApplications;
//use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class CollectionController extends Controller
{
    public function readCollection() {
        return response()->json(CollectionOfApplications::get(), 200);
    }

    public function readCollectionById($id) {
        return response()->json(CollectionOfApplications::find($id), 200);
    }

    public function createCollections(Request $req) {

        $rules = [
            'phone' => 'min:11, max:11
                        |integer|unique:collection_of_applications,phone',
            'name' => 'required|min:3'
        ];
        $validator = Validator::make($req->all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $collections = CollectionOfApplications::create($req->all());
        return response()->json(['massage' => 'Thank you for registering', 'collections' => $collections], 200);
    }
}
